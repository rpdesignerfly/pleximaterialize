
// -----------------------------------------------------------
//
// MATERIALIZE JS
//

// O trecho abaixo é uma cópia personalizada do importador original do materialize
// que pode ser encontrado em node_modules/materialize-css/js/
// Isso permite personalizar os módulos do framework, diminuindo o tamanho final

// Inicializador

require('materialize-css/js/global.js'); 

// Componentes

require('materialize-css/js/cards.js');
require('materialize-css/js/toasts.js');
require('materialize-css/js/tabs.js');
require('materialize-css/js/tooltip.js');
require('materialize-css/js/buttons.js');
require('materialize-css/js/dropdown.js');
require('materialize-css/js/waves.js');
require('materialize-css/js/modal.js');
require('materialize-css/js/collapsible.js');
require('materialize-css/js/chips.js');
require('materialize-css/js/materialbox.js');
require('materialize-css/js/forms.js');
// ... table_of_contents
require('materialize-css/js/sideNav.js');
// ... preloader
require('materialize-css/js/slider.js');
require('materialize-css/js/carousel.js');
require('materialize-css/js/tapTarget.js');
// ... pulse
require('materialize-css/js/date_picker/picker.js');
// require('materialize-css/js/date_picker/picker.date.js'); <-- não compila ??

// Extras Materialize

require('materialize-css/js/scrollFire.js');
require('materialize-css/js/transitions.js');


// Extras Independentes

require('materialize-css/js/character_counter.js');
require('materialize-css/js/parallax.js');
require('materialize-css/js/prism.js');
require('materialize-css/js/pushpin.js');
require('materialize-css/js/scrollspy.js');
require('materialize-css/js/waves.js');
