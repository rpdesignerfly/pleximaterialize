
//
//--------------------------------------------------------------------------
// Javascript Assets
//--------------------------------------------------------------------------
//
// Este arquivo é invocado pelo LaravelMix e gera um único arquivo javascript compactado.
// Todas as dependencias invocadas aqui serão compiladas com base no arquivo 'webpack.mix.js'
//

// 1. Funções e Bibliotecas básicas
// ==========================================================================

require('./_libraries.js');


// 2. Framework
// ==========================================================================

require('./materialize/_components.js');


// 3. Implementações Extras
// ==========================================================================

require('./_extras.js');
