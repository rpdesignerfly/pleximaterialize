
// -----------------------------------------------------------
//
// IMPLEMENTAÇÕES EXTRAS JS
//

// 1. Materialize SideNav
// ==========================================================================

$(document).ready(function() {
    $(".button-collapse").sideNav();
});

// 2. Materialize Carousel
// ==========================================================================

var carousel_autoplay = [];
window.carouselAutoplay = function(carouselElem)
{
    var index = $(carouselElem).index();

    if (undefined !== carousel_autoplay[index]) {
        clearInterval(carousel_autoplay[index]);
    }

    var delay = (undefined !== $(carouselElem).data('delay'))
        ? $(carouselElem).data('delay')
        : 4500;

    carousel_autoplay[index] = setInterval(function(){
        $(carouselElem).carousel('next');
    }, delay);
};

$(document).ready(function() {

    $('.carousel.carousel-gallery').carousel();
    $('.carousel.carousel-slider').carousel({ fullWidth: true });

    $('.carousel.autoplay').each(function() {
        carouselAutoplay(this);
    }); 

    $('.carousel').each(function() {
        var self = this;
        $('.carousel-control-left').click(function(){ carouselAutoplay(self); $(self).carousel('prev'); });
        $('.carousel-control-right').click(function(){ carouselAutoplay(self); $(self).carousel('next'); });
    }); 
});




// 2. Ajax Laravel
// ==========================================================================

// A configuração abaixo prepara automaticamente todas as requisições ajax 
// do sistema para enviarem o token CSRF como um cabeçalho a fim de 
// validar as requisições para os formulários.

$.ajaxSetup({
    beforeSend: function (xhr)
    {
        if (undefined !== $('meta[name="csrf-token"]').get(0)) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        }
        //xhr.setRequestHeader("X-CSRF-TOKEN", window.Laravel.csrfToken);
        xhr.setRequestHeader("X-Requested-With","XMLHttpRequest");
    }
});
