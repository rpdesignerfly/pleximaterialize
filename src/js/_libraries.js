
// -----------------------------------------------------------
//
// BIBLIOTECAS
//

// 1. JQuery (Importante: versão do materialize)
// ==========================================================================

//window.$ = window.jQuery = require('materialize-css/node_modules/jquery/dist/jquery.js'); 
window.$ = window.jQuery = require('jquery'); 

// 2. Dependencias do materialize
// ==========================================================================

require('materialize-css/js/hammer.min.js');
require('materialize-css/js/jquery.easing.1.3.js');
require('materialize-css/js/velocity.min.js');
require('materialize-css/js/jquery.hammer.js');
require('materialize-css/js/jquery.timeago.min.js');
require('materialize-css/js/animation.js');


// require('jquery-autosize');
// require('jquery.maskedinput/src/jquery.maskedinput.js');
// require('jquery-maskmoney/dist/jquery.maskMoney.js');

